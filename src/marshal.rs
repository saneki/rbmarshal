// Ruby's marshal source: `marshal.c` (https://github.com/ruby/ruby/blob/trunk/marshal.c)
use core::convert::TryFrom;
use core::f64;
use core::fmt;
use core::result;

pub mod de;

use crate::error::de::{Error, Result};

use pretty::{BoxDoc, Doc};
use strum_macros::{AsStaticStr, IntoStaticStr};

/// Width used for pretty-printing with pretty::Doc.
static DOC_WIDTH: usize = 200;

struct Float(Vec<u8>);

impl Float {
    /// Create from bytes.
    pub fn from_bytes(bytes: &[u8]) -> Self {
        Self(Vec::from(bytes))
    }

    /// Attempt to parse this data as `f64`.
    pub fn parse(&self) -> Result<f64> {
        let san = self.sanitized();
        let text = String::from_utf8(san.to_vec());
        match text {
            Ok(text) => {
                let special = Float::parse_special(&text);
                match special {
                    Some(val) => Ok(val),
                    None => {
                        let parsed = text.parse::<f64>();
                        match parsed {
                            Ok(val) => Ok(val),
                            _ => Err(Error::ParseFloat(text)),
                        }
                    }
                }
            }
            _ => Err(Error::MalformedFloat(san.to_vec())),
        }
    }

    /// Try to parse a special float value from a string.
    pub fn parse_special(string: &str) -> Option<f64> {
        match string {
            "0" => Some(0.0),
            "-0" => Some(-0.0),
            "inf" => Some(f64::INFINITY),
            "-inf" => Some(f64::NEG_INFINITY),
            "nan" => Some(f64::NAN),
            _ => None,
        }
    }

    /// Get a slice containing the sanitized data.
    pub fn sanitized(&self) -> &[u8] {
        self.split().0
    }

    /// Split the float data between "sanitized" and "junk" data.
    ///
    /// This returns two slices:
    /// - Up until the first null-terminating byte.
    /// - The null-terminating byte, and everything after.
    ///
    /// Apparently, the `w_float` implementation in `marshal.c` may also write the
    /// null-terminating byte and some junk data after it.
    pub fn split(&self) -> (&[u8], &[u8]) {
        for i in 0..self.0.len() {
            if self.0[i] == 0 {
                return (&self.0[0..i], &self.0[i..]);
            }
        }

        (&self.0[..], &self.0[0..0])
    }
}

/// Ruby marshal header containing major and minor version values.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Header {
    /// Major version value
    pub major: u8,
    /// Minor version value
    pub minor: u8,
}

impl Default for Header {
    fn default() -> Self {
        Self { major: 4, minor: 8 }
    }
}

impl Header {
    pub fn new(major: u8, minor: u8) -> Self {
        Self {
            major: major,
            minor: minor,
        }
    }

    pub fn validate(&self) -> Result<()> {
        if self == &Header::default() {
            Ok(())
        } else {
            Err(Error::HeaderVersion(*self))
        }
    }
}

/// Some information indicating string encoding.
pub struct Encoding<'a>(&'a str, bool);

/// IVar data, mapping a Symbol to an Element.
#[derive(Clone, Debug)]
pub struct IVar(Box<Symbol>, Box<Element>);

impl IVar {
    /// Get the value as an Element.
    pub fn element(&self) -> &Element {
        &self.1
    }

    /// Get the value as a bool.
    pub fn is(&self) -> Result<bool> {
        let ele = self.element();
        match ele {
            Element::Bool(val) => Ok(*val),
            _ => Err(Error::MalformedEncodingBool(ele.id())),
        }
    }

    /// Validate the name and return the bool value.
    pub fn is_default_encoding(&self) -> Result<bool> {
        // Encoding `{ :"E", true }` should return `true`.
        // Encoding `{ :"E", false }` should return `false`.
        // For now, everything else should return an Error?
        self.validate_encoding_name()?;
        self.is()
    }

    /// Get the name as a `&str`.
    pub fn name(&self) -> Result<&str> {
        match &self.0.name {
            MaybeString::Str(name) => Ok(name.as_ref()),
            _ => Err(Error::MalformedEncodingName(None)),
        }
    }

    /// Get a pretty::Doc representation for pretty-printing.
    pub fn to_doc(&self) -> Doc<BoxDoc<()>> {
        Doc::text("(")
            .append(self.symbol().to_doc())
            .append(" => ")
            .append(self.element().to_doc())
            .append(")")
            .group()
    }

    /// Get this state as encoding data.
    pub fn to_encoding(&self) -> Result<Encoding> {
        let name = self.name()?;
        let value = self.is_default_encoding()?;
        Ok(Encoding(name, value))
    }

    /// Validate that the name is the expected `"E"`.
    pub fn validate_encoding_name(&self) -> Result<()> {
        let name = self.name()?;
        match name {
            "E" => Ok(()),
            _ => Err(Error::MalformedEncodingName(Some(String::from(name)))),
        }
    }

    /// Get the name as a Symbol.
    pub fn symbol(&self) -> &Symbol {
        &self.0
    }
}

impl fmt::Display for IVar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.to_doc().render_fmt(DOC_WIDTH, f)
    }
}

/// Alias for ruby's concept of "long".
pub type Long = isize;

/// Ruby marshal type byte identifier, named corresponding to the constants
/// defined in ruby's `marshal.c` source file.
#[derive(Debug, Eq, PartialEq, AsStaticStr, IntoStaticStr)]
#[repr(u8)]
pub enum Id {
    Array = '[' as u8,
    BigNum = 'l' as u8,
    Class = 'c' as u8,
    Data = 'd' as u8,
    Extended = 'e' as u8,
    False = 'F' as u8,
    FixNum = 'i' as u8,
    Float = 'f' as u8,
    Hash = '{' as u8,
    HashDef = '}' as u8,
    IVar = 'I' as u8,
    Link = '@' as u8,
    Module = 'm' as u8,
    ModuleOld = 'M' as u8,
    Nil = '0' as u8,
    Object = 'o' as u8,
    RegExp = '/' as u8,
    String = '"' as u8,
    Struct = 'S' as u8,
    Symbol = ':' as u8,
    Symlink = ';' as u8,
    True = 'T' as u8,
    UClass = 'C' as u8,
    UserDef = 'u' as u8,
    UsrMarshal = 'U' as u8,
}

/// Todo: Write a macro for this later, maybe as an extension of the "derivatives" crate.
impl TryFrom<u8> for Id {
    type Error = Error;

    fn try_from(value: u8) -> result::Result<Self, Self::Error> {
        if value == (Id::Array as u8) {
            return Ok(Id::Array);
        } else if value == (Id::BigNum as u8) {
            return Ok(Id::BigNum);
        } else if value == (Id::Class as u8) {
            return Ok(Id::Class);
        } else if value == (Id::Data as u8) {
            return Ok(Id::Data);
        } else if value == (Id::Extended as u8) {
            return Ok(Id::Extended);
        } else if value == (Id::False as u8) {
            return Ok(Id::False);
        } else if value == (Id::FixNum as u8) {
            return Ok(Id::FixNum);
        } else if value == (Id::Float as u8) {
            return Ok(Id::Float);
        } else if value == (Id::Hash as u8) {
            return Ok(Id::Hash);
        } else if value == (Id::HashDef as u8) {
            return Ok(Id::HashDef);
        } else if value == (Id::IVar as u8) {
            return Ok(Id::IVar);
        } else if value == (Id::Link as u8) {
            return Ok(Id::Link);
        } else if value == (Id::Module as u8) {
            return Ok(Id::Module);
        } else if value == (Id::ModuleOld as u8) {
            return Ok(Id::ModuleOld);
        } else if value == (Id::Nil as u8) {
            return Ok(Id::Nil);
        } else if value == (Id::Object as u8) {
            return Ok(Id::Object);
        } else if value == (Id::RegExp as u8) {
            return Ok(Id::RegExp);
        } else if value == (Id::String as u8) {
            return Ok(Id::String);
        } else if value == (Id::Struct as u8) {
            return Ok(Id::Struct);
        } else if value == (Id::Symbol as u8) {
            return Ok(Id::Symbol);
        } else if value == (Id::Symlink as u8) {
            return Ok(Id::Symlink);
        } else if value == (Id::True as u8) {
            return Ok(Id::True);
        } else if value == (Id::UClass as u8) {
            return Ok(Id::UClass);
        } else if value == (Id::UserDef as u8) {
            return Ok(Id::UserDef);
        } else if value == (Id::UsrMarshal as u8) {
            return Ok(Id::UsrMarshal);
        } else {
            return Err(Error::InvalidType(value));
        }
    }
}

#[derive(Clone, Debug)]
pub enum MaybeString {
    Str(String),
    Data(Vec<u8>),
}

impl MaybeString {
    pub fn new(data: Vec<u8>) -> Self {
        match String::from_utf8(data.clone()) {
            Ok(s) => MaybeString::Str(s),
            Err(_) => MaybeString::Data(data),
        }
    }

    /// Try and get the string data, and fallback to a default value if none.
    pub fn string_or<'a>(&'a self, default: &'a str) -> &'a str {
        match self {
            Self::Str(s) => s,
            _ => default,
        }
    }
}

impl fmt::Display for MaybeString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            // If string, show string by itself.
            Self::Str(s) => write!(f, "{}", s),
            // If bytes, show debug string for now.
            Self::Data(b) => write!(f, "{:?}", b),
        }
    }
}

/// Symbol data.
#[derive(Clone, Debug)]
pub struct Symbol {
    name: MaybeString,
    ivar: Option<Vec<IVar>>,
}

impl Symbol {
    pub fn from(name: Vec<u8>, ivar: Option<Vec<IVar>>) -> Self {
        Self {
            name: MaybeString::new(name),
            ivar: ivar,
        }
    }

    pub fn to_doc(&self) -> Doc<BoxDoc<()>> {
        match &self.name {
            MaybeString::Str(val) => Doc::text(":").append("\"").append(val).append("\"").group(),
            MaybeString::Data(val) => Doc::text(format!("{:?}", val)).group(),
        }
    }
}

impl fmt::Display for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.to_doc().render_fmt(DOC_WIDTH, f)
    }
}

#[derive(Clone, Debug)]
pub enum Element {
    Array(Vec<Element>),
    BigNum(u8, Long, Vec<u8>),
    Bool(bool),
    Class(MaybeString),
    Extended(Symbol, Box<Element>),
    FixNum(Long),
    Float(f64),
    Hash(Vec<(Element, Element)>),
    IVar(Box<Element>, Option<Vec<IVar>>),
    /// Link points to an entry stored in `arg->data`.
    /// For now just storing the index for convenience.
    Link(Long),
    /// Nil value.
    Nil,
    Object(Symbol, Vec<IVar>),
    RegExp(String, u8),
    String(MaybeString),
    Symbol(Symbol),
    Symlink(Long),
    UserDef(Symbol, Symbol),
    UsrMarshal(Symbol, Box<Element>),
}

impl Element {
    /// Get the Id corresponding to this Element.
    pub fn id(&self) -> Id {
        match self {
            Self::Array(_) => Id::Array,
            Self::BigNum(_, _, _) => Id::BigNum,
            Self::Bool(val) => match val {
                true => Id::True,
                false => Id::False,
            },
            Self::Class(_) => Id::Class,
            Self::Extended(_, _) => Id::Extended,
            Self::FixNum(_) => Id::FixNum,
            Self::Float(_) => Id::Float,
            Self::Hash(_) => Id::Hash,
            Self::IVar(_, _) => Id::IVar,
            Self::Link(_) => Id::Link,
            Self::Nil => Id::Nil,
            Self::Object(_, _) => Id::Object,
            Self::RegExp(_, _) => Id::RegExp,
            Self::String(_) => Id::String,
            Self::Symbol(_) => Id::Symbol,
            Self::Symlink(_) => Id::Symlink,
            Self::UserDef(_, _) => Id::UserDef,
            Self::UsrMarshal(_, _) => Id::UsrMarshal,
        }
    }

    /// Corresponds to the `enc_capable` function in Ruby's `encoding.c` source file.
    pub fn enc_capable(&self) -> bool {
        // TODO: This function is unfinished?
        match self.id() {
            Id::String | Id::RegExp | Id::Symbol => true,
            _ => false,
        }
    }

    pub fn to_doc(&self) -> Doc<BoxDoc<()>> {
        match self {
            Element::Array(arr) => Doc::text("[")
                .append(
                    Doc::intersperse(
                        arr.into_iter().map(|x| x.to_doc()),
                        Doc::text(",").append(Doc::newline()),
                    )
                    .nest(1),
                )
                .append("]")
                .group(),
            Element::Bool(val) => {
                // Show "True" or "False" depending on value
                match val {
                    true => Doc::text("True").group(),
                    false => Doc::text("False").group(),
                }
            }
            Element::FixNum(val) => {
                // Show long value as string
                Doc::as_string(val).group()
            }
            Element::Float(val) => {
                // Show float value as string
                Doc::as_string(val).group()
            }
            Element::Hash(map) => Doc::text("{{")
                .append(
                    Doc::intersperse(
                        map.into_iter()
                            .map(|(k, v)| k.to_doc().append(" => ").append(v.to_doc())),
                        Doc::text(",").append(Doc::newline()),
                    )
                    .nest(1),
                )
                .append("}}")
                .group(),
            Element::IVar(val, _) => {
                // Ignore the encoding
                val.to_doc().group()
            }
            Element::Object(sym, ivars) => Doc::text("Object")
                .append("(")
                .append(Doc::space())
                .append(sym.to_doc())
                .append(Doc::newline())
                .append(
                    Doc::intersperse(
                        ivars.into_iter().map(|x| x.to_doc()),
                        Doc::text(",").append(Doc::newline()),
                    )
                    .nest(1),
                )
                .append(")")
                .group(),
            Element::String(val) => {
                // Display string for MaybeString
                Doc::text("\"")
                    .append(Doc::as_string(val))
                    .append("\"")
                    .group()
            }
            Element::Symbol(sym) => {
                // Display symbol value without the wrapping
                sym.to_doc().group()
            }
            Element::UserDef(sym1, sym2) => Doc::text("UserDef")
                .append("(")
                .append(sym1.to_doc())
                .append(", ")
                .append(sym2.to_doc())
                .append(")")
                .group(),
            Element::UsrMarshal(sym, obj) => Doc::text("UsrMarshal")
                .append("(")
                .append(format!("{}", sym))
                .append(", ")
                .append(obj.to_doc())
                .append(")")
                .group(),
            // For all other elements, get debug text?
            _ => Doc::text(format!("{:?}", self)),
        }
    }
}

impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.to_doc().render_fmt(DOC_WIDTH, f)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_element_id() {
        use super::{Element, Id, MaybeString, Symbol};
        assert_eq!(Id::Array, Element::Array(vec![]).id());
        assert_eq!(Id::BigNum, Element::BigNum(0, 0, vec![]).id());
        assert_eq!(Id::Class, Element::Class(MaybeString::new(vec![])).id());
        assert_eq!(Id::False, Element::Bool(false).id());
        assert_eq!(Id::FixNum, Element::FixNum(0).id());
        assert_eq!(Id::Float, Element::Float(0.0).id());
        assert_eq!(Id::IVar, Element::IVar(Box::new(Element::Nil), None).id());
        assert_eq!(Id::Nil, Element::Nil.id());
        assert_eq!(Id::String, Element::String(MaybeString::new(vec![])).id());
        assert_eq!(Id::Symbol, Element::Symbol(Symbol::from(vec![], None)).id());
        assert_eq!(Id::True, Element::Bool(true).id());
    }

    #[test]
    fn test_float_parse() {
        use super::Float;

        let data = "0.1234".as_bytes();
        let value = Float::from_bytes(data);
        assert_eq!(value.parse().unwrap(), 0.1234);
        assert_eq!(value.split().0, data);
        assert!(value.split().1.len() == 0); // Assert no junk data

        let data = "-9999.5678".as_bytes();
        let value = Float::from_bytes(data);
        assert_eq!(value.parse().unwrap(), -9999.5678);
        assert_eq!(value.split().0, data);
        assert!(value.split().1.len() == 0); // Assert no junk data
    }

    #[test]
    fn test_float_parse_special() {
        use super::Float;
        use core::f64;

        let pos_zero = Float::parse_special("0").unwrap();
        let neg_zero = Float::parse_special("-0").unwrap();

        // Test positive-0 value and sign
        assert_eq!(pos_zero, 0.0);
        assert_eq!(pos_zero.signum(), 1.0);

        // Test negative-0 value and sign
        assert_eq!(neg_zero, 0.0);
        assert_eq!(neg_zero.signum(), -1.0);

        // Test "infinity" values: inf/-inf
        assert_eq!(Float::parse_special("inf"), Some(f64::INFINITY));
        assert_eq!(Float::parse_special("-inf"), Some(f64::NEG_INFINITY));

        // Test NAN
        let nan = Float::parse_special("nan").unwrap();
        assert!(nan.signum().is_nan());
    }
}
