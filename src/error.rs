pub mod de {
    use crate::marshal::{Header, Id, Long};
    use core::fmt;
    use core::result;

    use strum::AsStaticRef;

    #[derive(Debug)]
    pub enum Error {
        /// End of File
        Eof,
        /// The version specified in the header is not supported.
        HeaderVersion(Header),
        /// The type character is invalid.
        InvalidType(u8),
        /// Malformed encoding element type when expecting bool.
        MalformedEncodingBool(Id),
        /// Malformed encoding name, or None if name cannot be parsed as a String.
        MalformedEncodingName(Option<String>),
        /// Malformed float data.
        MalformedFloat(Vec<u8>),
        /// The encoded long value is malformed.
        MalformedLong(i8),
        /// Float parsing error.
        ParseFloat(String),
        /// Symbol type error.
        SymbolType(Id),
        /// Symlink error.
        Symlink(Long),
        /// Symlink entry contains an unexpected encoding.
        SymlinkWithEncoding,
    }

    impl fmt::Display for Error {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match &self {
                Error::Eof => write!(f, "Reached EOF (end-of-file)"),
                Error::HeaderVersion(hdr) => {
                    write!(f, "Bad header version: {}.{}", hdr.major, hdr.minor)
                }
                Error::InvalidType(typ) => write!(f, "Invalid type byte: {}", typ),
                Error::MalformedEncodingBool(id) => write!(
                    f,
                    "Malformed encoding, expected Bool but found: {}",
                    id.as_static()
                ),
                Error::MalformedEncodingName(name) => match name {
                    Some(name) => write!(f, "Unknown encoding name: \"{}\"", name),
                    None => write!(f, "Unable to decode encoding name"),
                },
                Error::MalformedFloat(bytes) => write!(f, "Malformed float data: {:?}", bytes),
                Error::MalformedLong(val) => write!(f, "Malformed long value: {}", val),
                Error::ParseFloat(string) => write!(f, "Unable to parse float: \"{}\"", string),
                Error::SymbolType(id) => write!(f, "Unexpected symbol type: {}", id.as_static()),
                Error::Symlink(idx) => write!(f, "Symlink index error: {}", idx),
                Error::SymlinkWithEncoding => {
                    write!(f, "Unexpected Symlink with specified encoding")
                }
            }
        }
    }

    pub type Result<T> = result::Result<T, Error>;
}
