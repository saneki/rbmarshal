use core::convert::TryFrom;

use log;

use crate::error::de::{Error, Result};
use crate::marshal::{Element, Float, Header, IVar, Id, Long, MaybeString, Symbol};

// use serde::de::{self, Deserialize};

pub struct Depth(Option<usize>);

impl Depth {
    /// Get a new depth without a current value.
    pub fn new() -> Self {
        Depth(None)
    }

    /// Decrement depth, going from Some to None if necessary.
    pub fn decr(&mut self) {
        self.0 = match self.0.unwrap() {
            0 => None,
            _ => Some(self.0.unwrap() - 1),
        };
    }

    /// Increment depth, going from None to Some if necessary.
    pub fn incr(&mut self) {
        self.0 = match self.0 {
            Some(depth) => Some(depth + 1),
            None => Some(0),
        };
    }

    /// Get the current depth value.
    pub fn value(&self) -> Option<usize> {
        self.0
    }
}

/// Emitter for emitting objects as they are deserialized.
pub trait Emitter {
    fn emit(&self, data: &Element, depth: usize, start: usize, end: usize);
}

pub struct DummyEmitter;

impl DummyEmitter {
    pub fn new() -> Self {
        Self
    }
}

impl Emitter for DummyEmitter {
    fn emit(&self, _: &Element, _: usize, _: usize, _: usize) {
        // Do nothing
    }
}

/// Emitter for logging objects via the `log` module.
pub struct LogEmitter {
    level: log::Level,
}

impl LogEmitter {
    pub fn new() -> Self {
        LogEmitter::with_level(log::Level::Info)
    }

    pub fn with_level(level: log::Level) -> Self {
        Self { level: level }
    }
}

impl Emitter for LogEmitter {
    fn emit(&self, data: &Element, depth: usize, start: usize, end: usize) {
        log::log!(
            self.level,
            "Emit<{},({:X}:{:X})> {}",
            depth,
            start,
            end,
            data
        );
    }
}

pub struct Deserializer<'de, E>
where
    E: Emitter,
{
    depth: Depth,
    emitter: Option<&'de E>,
    /// Effective offset (this may not be the best naming).
    /// This points to the start of the data currently being examined.
    eoffset: Vec<usize>,
    input: &'de [u8],
    offset: usize,
    symbols: Vec<Symbol>,
}

macro_rules! log_parse_type {
    ($name:expr, $slf:expr) => {
        log::trace!(target: "deserialize", "Parsing[{} : 0x{:X}]: {}",
            $slf.depth.value().unwrap(), $slf.get_offset().unwrap(), $name)
    };
}

/// Convenience function to prevent users from having to specify the emitter generic type when not
/// using an emitter.
pub fn deserializer<'de>(input: &'de [u8]) -> Deserializer<'de, DummyEmitter> {
    Deserializer::from_bytes(input)
}

/// Deserializer for ruby marshal data.
impl<'de, E> Deserializer<'de, E>
where
    E: Emitter,
{
    /// Create from some bytes and an emitter.
    pub fn new(input: &'de [u8], emitter: Option<&'de E>) -> Self {
        Self {
            depth: Depth::new(),
            emitter: emitter,
            eoffset: Vec::new(),
            input: input,
            offset: 0,
            symbols: Vec::new(),
        }
    }

    /// Whether or not any input remains.
    pub fn is_eof(&self) -> bool {
        self.input.len() == 0
    }

    /// Create from some bytes without an emitter.
    pub fn from_bytes(input: &'de [u8]) -> Self {
        Self::new(input, None)
    }

    /// Get the current offset.
    pub fn get_offset(&self) -> Option<usize> {
        self.eoffset.last().cloned()
    }

    /// Peek at the next byte in the input without consuming it.
    fn peek_byte(&self) -> Result<u8> {
        match self.input.get(0) {
            Some(x) => Ok(*x),
            None => Err(Error::Eof),
        }
    }

    /// Consume the next byte in the input.
    fn read_byte(&mut self) -> Result<u8> {
        let x = self.peek_byte()?;
        self.input = &self.input[1..];
        self.offset += 1;
        Ok(x)
    }

    /// Read a length field and then the following bytes.
    fn read_bytes(&mut self) -> Result<Vec<u8>> {
        let len = self.read_long()?;
        self.read_bytes_with_len(len)
    }

    /// Read a specific amount of bytes.
    fn read_bytes_with_len(&mut self, len: Long) -> Result<Vec<u8>> {
        match len {
            i if i >= 0 => {
                let length = len as usize;
                let slice = &self.input[..length];
                let bytes = Vec::from(slice);
                self.input = &self.input[length..];
                self.offset += length;
                Ok(bytes)
            }
            // If len is negative, do nothing and return empty vec?
            _ => Ok(Vec::new()),
        }
    }

    /// Emit some data via the emitter.
    fn emit(&self, data: &Element, depth: usize, start: usize, end: usize) {
        match self.emitter {
            Some(emitter) => emitter.emit(data, depth, start, end),
            None => (),
        }
    }

    /// Read the header struct.
    pub fn read_header(&mut self) -> Result<Header> {
        // Push our current offset as the effective offset
        self.eoffset.push(self.offset);

        let major = self.read_byte()?;
        let minor = self.read_byte()?;

        // Pop the effective offset
        self.eoffset.pop();

        Ok(Header::new(major, minor))
    }

    /// Read the header and validate the version.
    pub fn read_validate_header(&mut self) -> Result<Header> {
        let header = self.read_header()?;
        header.validate()?;
        Ok(header)
    }

    /// Read a root object, including the preceding header.
    pub fn read_root_object(&mut self) -> Result<(Header, Element)> {
        let header = self.read_validate_header()?;
        let obj = self.read_object(false)?;
        Ok((header, obj))
    }

    /// Read an object.
    pub fn read_object(&mut self, in_ivar: bool) -> Result<Element> {
        // Push our current offset as the effective offset
        self.eoffset.push(self.offset);
        // Increment depth
        self.depth.incr();

        let result = self.read_object_inner(in_ivar)?;
        let depth = self.depth.value().unwrap();

        // Decrement depth
        self.depth.decr();
        // Pop the effective offset
        let eoffset = self.eoffset.pop();

        // Emit result via emitter
        self.emit(&result, depth, eoffset.unwrap(), self.offset);

        Ok(result)
    }

    /// Internal method for reading an object.
    fn read_object_inner(&mut self, in_ivar: bool) -> Result<Element> {
        let c = self.read_byte()?;
        let e = Id::try_from(c);

        match e {
            Ok(Id::Array) => {
                // Parse Array
                log_parse_type!("Array", self);
                let mut arr = Vec::new();
                let len = self.read_long()?;
                for _ in 0..len {
                    let obj = self.read_object(false)?;
                    arr.push(obj);
                }
                Ok(Element::Array(arr))
            }
            Ok(Id::BigNum) => {
                // Parse BigNum
                log_parse_type!("BigNum", self);
                let sign = self.read_byte()?;
                let len = self.read_long()?;
                let data = self.read_bytes_with_len(len * 2)?;
                Ok(Element::BigNum(sign, len, data))
            }
            Ok(Id::Class) => {
                // Parse Class
                log_parse_type!("Class", self);
                let path = self.read_bytes()?;
                Ok(Element::Class(MaybeString::new(path)))
            }
            Ok(Id::Extended) => {
                // Parse Extended
                log_parse_type!("Extended", self);
                let sym = self.read_symbol()?;
                let obj = self.read_object(false)?;
                Ok(Element::Extended(sym, Box::new(obj)))
            }
            Ok(Id::False) => {
                // Parse False
                log_parse_type!("False", self);
                let val = Element::Bool(false);
                Ok(val)
            }
            Ok(Id::FixNum) => {
                // Parse Fixnum
                log_parse_type!("Fixnum", self);
                let val = self.read_long()?;
                Ok(Element::FixNum(val))
            }
            Ok(Id::Float) => {
                // Parse Float
                log_parse_type!("Float", self);
                let bytes = self.read_bytes()?;
                let float = Float::from_bytes(&bytes);
                Ok(Element::Float(float.parse()?))
            }
            Ok(Id::Hash) | Ok(Id::HashDef) => {
                // Parse Hash
                log_parse_type!("Hash", self);
                let mut map = Vec::new();
                let len = self.read_long()?;
                for _ in 0..len {
                    let key = self.read_object(false)?;
                    let val = self.read_object(false)?;
                    map.push((key, val));
                }
                Ok(Element::Hash(map))
            }
            Ok(Id::IVar) => {
                // Parse IVar
                log_parse_type!("IVar", self);
                let obj = self.read_object(true)?;
                match obj {
                    // Referencing `marshal.c`:
                    // When in an IVar, reading a RegExp, Symbol or UserDef will seemingly always set
                    // the `ivp` flag (pointer) to false. If the flag is true after reading, only then
                    // does `r_object0` call `r_ivar`.
                    Element::RegExp(_, _) | Element::Symbol(_) | Element::UserDef(_, _) => {
                        Ok(Element::IVar(Box::new(obj), None))
                    }
                    _ => {
                        let data = self.read_ivar()?;
                        Ok(Element::IVar(Box::new(obj), Some(data)))
                    }
                }
            }
            Ok(Id::Link) => {
                // Parse Link
                log_parse_type!("Link", self);
                let idx = self.read_long()?;
                Ok(Element::Link(idx))
            }
            Ok(Id::Nil) => {
                // Parse Nil
                log_parse_type!("Nil", self);
                Ok(Element::Nil)
            }
            Ok(Id::Object) => {
                // Parse Object
                log_parse_type!("Object", self);
                let sym = self.read_symbol()?;
                let ivar = self.read_ivar()?;
                Ok(Element::Object(sym, ivar))
            }
            Ok(Id::RegExp) => {
                // Parse RegExp
                log_parse_type!("RegExp", self);
                let bytes = self.read_bytes()?;
                let options = self.read_byte()?;
                if in_ivar {
                    self.read_ivar()?;
                }
                let text = String::from_utf8(bytes).unwrap();
                Ok(Element::RegExp(text, options))
            }
            Ok(Id::String) => {
                // Parse String
                log_parse_type!("String", self);
                let bytes = self.read_bytes()?;
                Ok(Element::String(MaybeString::new(bytes)))
            }
            Ok(Id::Symbol) => {
                // Parse Symbol
                log_parse_type!("Symbol", self);
                let sym = self.read_symreal(in_ivar)?;
                Ok(Element::Symbol(sym))
            }
            Ok(Id::Symlink) => {
                // Parse Symlink
                // Transparently treat Symlink data as the Symbol it points to.
                log_parse_type!("Symlink", self);
                let sym = self.read_symlink()?;
                Ok(Element::Symbol(sym))
            }
            Ok(Id::True) => {
                // Parse True
                log_parse_type!("True", self);
                let val = Element::Bool(true);
                Ok(val)
            }
            Ok(Id::UserDef) => {
                // Parse UserDef
                log_parse_type!("UserDef", self);
                let sym1 = self.read_symbol()?;
                let sym2 = self.read_symreal(in_ivar)?;
                Ok(Element::UserDef(sym1, sym2))
            }
            Ok(Id::UsrMarshal) => {
                // Parse UsrMarshal
                log_parse_type!("UsrMarshal", self);
                let path = self.read_symbol()?;
                let obj = self.read_object(false)?;
                Ok(Element::UsrMarshal(path, Box::new(obj)))
            }
            _ => Err(Error::InvalidType(c)),
        }
    }

    /// Read IVar data.
    fn read_ivar(&mut self) -> Result<Vec<IVar>> {
        let mut vec = Vec::new();
        let len = self.read_long()?;
        for _ in 0..len {
            let sym = self.read_symbol()?; // Encoding
            let obj = self.read_object(false)?;
            vec.push(IVar(Box::new(sym), Box::new(obj)));
        }
        Ok(vec)
    }

    /// Read Symlink data and return a clone of the corresponding Symbol.
    fn read_symlink(&mut self) -> Result<Symbol> {
        let idx = self.read_long()?;
        let sym = self.symbols.get(idx as usize);
        match sym {
            // Todo: Maybe return reference?
            // For now, just return a clone of the Symbol instead of a reference.
            Some(sym) => Ok(sym.clone()),
            None => Err(Error::Symlink(idx)),
        }
    }

    /// Read a Symbol.
    fn read_symreal(&mut self, in_ivar: bool) -> Result<Symbol> {
        let bytes = self.read_bytes()?;

        let sym = match in_ivar {
            true => {
                let ivar = self.read_ivar()?;
                Symbol::from(bytes, Some(ivar))
            }
            false => Symbol::from(bytes, None),
        };

        // Todo: Maybe push refs with Deserializer lifetime instead of cloning.
        self.symbols.push(sym.clone());
        Ok(sym)
    }

    /// Read a Symbol.
    fn read_symbol(&mut self) -> Result<Symbol> {
        let mut ivar = false;

        loop {
            let c = self.read_byte()?;
            let e = Id::try_from(c)?;

            match e {
                Id::IVar => ivar = true,
                Id::Symbol => {
                    let sym = self.read_symreal(ivar)?;
                    return Ok(sym);
                }
                Id::Symlink => {
                    if ivar {
                        return Err(Error::SymlinkWithEncoding);
                    } else {
                        let sym = self.read_symlink()?;
                        return Ok(sym);
                    }
                }
                _ => return Err(Error::SymbolType(e)),
            }
        }
    }

    /// Read a long.
    fn read_long(&mut self) -> Result<Long> {
        let c = self.read_byte()? as i8;

        match c {
            0 => {
                // 0 is a special value
                Ok(0)
            }
            1..=4 => {
                // The number of bytes to read for (positive) value
                let mut x: Long = 0;
                for i in 0..c {
                    x |= (self.read_byte()? as Long) << (i * 8);
                }
                Ok(x)
            }
            5..=127 => {
                // The (positive) value is encoded in this type
                Ok((c as Long) - 5)
            }
            -4..=-1 => {
                // The number of bytes to read for (negative) value
                let mut x: Long = -1;
                for i in 0..-c {
                    // Clear bits before bitwise OR
                    x &= !(0xFF << (i * 8));
                    x |= (self.read_byte()? as Long) << (i * 8);
                }
                Ok(x)
            }
            -128..=-5 => {
                // The (negative) value is encoded in this byte
                Ok((c as Long) + 5)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    /// Deserialize some bytes to a long value for testing.
    macro_rules! to_long {
        ( $( $x:expr ),* ) => {
            {
                let temp_bytes = vec![$(($x),)*];
                let mut temp_des = deserializer(&temp_bytes);
                temp_des.read_long().unwrap()
            }
        };
    }

    #[test]
    fn test_read_long() {
        use super::deserializer;

        // Single-byte
        assert_eq!(to_long![0], 0);
        assert_eq!(to_long![5], 0);
        assert_eq!(to_long![0xFB], 0); // -5
        assert_eq!(to_long![6], 1);
        assert_eq!(to_long![10], 5);
        assert_eq!(to_long![0xF6], -5); // -10
        assert_eq!(to_long![20], 15);
        assert_eq!(to_long![127], 122);
        assert_eq!(to_long![128], -123);

        // Multi-byte
        assert_eq!(to_long![1, 123], 123);
        assert_eq!(to_long![1, 128], 128);
        assert_eq!(to_long![1, 255], 255);
        assert_eq!(to_long![2, 0x34, 0x12], 0x1234);
        assert_eq!(to_long![3, 0x56, 0x34, 0x12], 0x123456);
        assert_eq!(to_long![4, 0x78, 0x56, 0x34, 0x12], 0x12345678);
        assert_eq!(to_long![0xFE, 0xCC, 0xED], -0x1234);
        assert_eq!(to_long![0xFD, 0xAA, 0xCB, 0xED], -0x123456);
        assert_eq!(to_long![0xFC, 0x88, 0xA9, 0xCB, 0xED], -0x12345678);
    }
}
