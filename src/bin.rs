use std::fs::File;
use std::io::Read;

mod error;
mod marshal;

use crate::marshal::de::deserializer;
// use crate::marshal::de::{Deserializer, LogEmitter};

use clap::{App, Arg};
use simplelog::{CombinedLogger, Config, LevelFilter, TermLogger, TerminalMode};

fn main() -> Result<(), std::io::Error> {
    CombinedLogger::init(vec![TermLogger::new(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Mixed,
    )
    .unwrap()])
    .unwrap();

    let matches = App::new("rbmarshal")
        .about("Utility for Ruby marshal-ed data.")
        .arg(
            Arg::with_name("FILE")
                .help("Input file.")
                .required(true)
                .index(1),
        )
        .get_matches();

    let path = matches.value_of("FILE").unwrap();
    let mut file = File::open(path)?;
    let mut data = Vec::new();
    file.read_to_end(&mut data)?;

    let mut des = deserializer(&data);

    // To use a more verbose emitter (via logging):
    // let emitter = LogEmitter::new();
    // let mut des = Deserializer::new(&data, Some(&emitter));

    while !des.is_eof() {
        let result = des.read_root_object();
        match result {
            Err(err) => {
                eprintln!("Error (offset: 0x{:X}): {}", des.get_offset().unwrap(), err);
                break;
            }
            Ok((_, obj)) => {
                println!("{}", obj);
            }
        }
    }

    Ok(())
}
